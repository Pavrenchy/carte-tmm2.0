using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using NeoAxis;

namespace Project
{
	public class FenetrePrincipale : NeoAxis.UIControl
	{
		
		Component_Scene scene = Component_Scene.First;
		UIControl Demarrage;
		UIControl Options;
		UIControl Parametrages;
		
		protected override void OnEnabledInSimulation()
		{
			//base.OnEnabledInSimulation();
			lancement();
		}
		protected override void OnUpdate( float delta )
		{
			base.OnUpdate(delta);
		}
		protected override void OnRenderUI( CanvasRenderer renderer )
		{
			base.OnRenderUI( renderer );
		}

		private void lancement()
		{
			//base.OnEnabledInSimulation();
			ScreenMessages.Add("Demarrage ");
			Demarrage = (UIControl)GetComponent<UIControl>("Demarrage");
			((UIButton)Demarrage.GetComponent<UIButton>("StartGame")).Click += ISButtonMode_Click;
			((UIButton)Demarrage.GetComponent<UIButton>("Options")).Click += ISButtonMode_Click;
			((UIButton)Demarrage.GetComponent<UIButton>("Exit")).Click += ISButtonQuitte_Click;
			
			ScreenMessages.Add("Demarrage 2");
			Options = (UIControl)GetComponent<UIControl>("Options");
			((UIButton)Options.GetComponent<UIButton>("Retour")).Click += ISButtonRetour_Click;


			ScreenMessages.Add("Demarrage 3");
			Parametrages = (UIControl)GetComponent<UIControl>("Parametrages");
			((UIButton)Parametrages.GetComponent<UIButton>("Start")).Click += ISButtonLaunch_Click;
			((UIButton)Parametrages.GetComponent<UIButton>("Retour")).Click += ISButtonRetour_Click;
			((UISlider)Parametrages.GetComponent<UISlider>("NbJoueurs")).ValueChanged += ISSlider_ValueChange;
			((UISlider)Parametrages.GetComponent<UISlider>("NbCartes")).ValueChanged += ISSlider_ValueChange;
			((UISlider)Parametrages.GetComponent<UISlider>("NbEchange")).ValueChanged += ISSlider_ValueChange;
			ScreenMessages.Add("Demarrage 4");
			Demarrage.Enabled = true;
		}
		void ISSlider_ValueChange(UISlider sender)
		{
			UIText text = (UIText)Parametrages.GetComponent<UIText>(sender.Name + "txt");
			if (text != null)
			{
				text.Text = sender.Value.ToString();
			}
			;
		}
		
		
		void ISButtonMode_Click(UIButton sender)
		{
			ScreenMessages.Add("Fonction Mode " + sender.Name);
			switch (sender.Name)
			{
				case ("StartGame"):
					{
						ScreenMessages.Add("Fonction Mode " + sender.Name);
						Demarrage.Enabled = false;
						Options.Enabled = false;
						Parametrages.Enabled = true;
						break;
					}
				case ("Options"):
					{
						Demarrage.Enabled = false;
						Options.Enabled = true;
						Parametrages.Enabled = false;
						break;
					}
				default:
					{
						Demarrage.Enabled = true;
						Options.Enabled = false;
						Parametrages.Enabled = false;
						break;
					}
			}
		}
		
		void ISButtonRetour_Click(UIButton sender)
		{
			ScreenMessages.Add("Fonction Retour " +sender.Name);
			Demarrage.Enabled = true;
			Options.Enabled = false;
			Parametrages.Enabled = false;
		}
		
		void ISButtonLaunch_Click(UIButton sender)
		{
			ScreenMessages.Add("Fonction Lancement " +sender.Name);
			var files = VirtualDirectory.GetFiles( "", "TableJeu.scene", SearchOption.AllDirectories );
			CollectionUtility.MergeSort( files, delegate ( string name1, string name2 )
				{
					var s1 = name1.Replace( "\\", " \\" ).Replace( "/", " /" );
					var s2 = name2.Replace( "\\", " \\" ).Replace( "/", " /" );
					return string.Compare( s1, s2 );
				} );
			var playFile = files[0];
			SimulationApp.PlayFile( playFile);
		}
		
		void ISButtonQuitte_Click(UIButton sender)
		{
			ScreenMessages.Add("Fonction Retour " +sender.Name);
			EngineApp.NeedExit = true;
		}

	}
}