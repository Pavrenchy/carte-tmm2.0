using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using NeoAxis;
using System.Linq;

namespace Project
{
	
	public class AdvisorSoviet : NeoAxis.UIControl
	{
		Component_Scene Scene;
		static System.Random rnd = new System.Random();
		ModeJeu JeuMode = ModeJeu.NonLance;
		GameML NewGame;
		UIControl CommentairePan;
		UIControl MisePan;
		UIControl MiseCachePan;
		Component_MeshInSpace CarteModelMain;
		int LongueurMain = 2;
		object touchDown;
		Vector2? touchPosition;
		String ObjetSelectionne;
		DebugInfo DebugInfo = new DebugInfo();
		DebugInfo.DebugMessage Message = new DebugInfo.DebugMessage();
		DebugInfo.ModeAffichage ModeAffichage = DebugInfo.ModeAffichage.Virtuel;
		DebugInfo.NiveauMessage DebugLevel = DebugInfo.NiveauMessage.Debug;
		List<Component_MeshInSpace> BceCarteIngry = new List<Component_MeshInSpace>();
		Component_MeshInSpace cartobjFontaine;
		Component_MeshInSpace CarteObjectifCollectif;
		double NT = EngineApp.EngineTime;
		double PT = EngineApp.EngineTime+5.0;
		double TimeGame = 0;
		Component_MeshInSpace goutte;
		bool Verif_temp;
		List<Component_RigidBody> LeveCarteList = new List<Component_RigidBody>();
		enum ModeJeu
		{
			NonLance,
			Initilisation,
			Commentaire,
			Confrontation,
			ConfrontationCachee,
			Resolution,
			MiseTapis,
			Attribution
		}
        protected override void OnEnabledInSimulation()
        {
            //base.OnEnabledInSimulation();
			lancement();
			InitializeSceneEvents();
			//FontaineCarte();
            
        }

        protected override void OnUpdate(float delta)
        {
			//base.OnUpdate(delta);
			//update_Fontaine();
			update_time();
		}
        
        
        
        protected override bool OnMouseDoubleClick(EMouseButtons button)
		{
			SourisScrut();
			return true;
		}
		
        private void ISButtonTake_Click(UIButton sender)
		{
			Scene = Component_Scene.First;
			if (Scene.Name == "TableJeu")
			{
				Carte MyfirstCarte = new Carte();
				MyfirstCarte.ObjectifCollectif = Carte.ObjCollectifs.Batiment2;
				MyfirstCarte.ObjectifPrive = Carte.ObjPrives.Bateau1;
				MyfirstCarte.Commentaire = Carte.Commentaires.Commentaire3;
				MyfirstCarte.Valeur = 5;
				Message.NvMess = DebugInfo.NiveauMessage.Debug;
				Message.DbgMess ="Nous sommes dans la bonne scene UI";
				DebugInfo.Add(ModeAffichage, Message, DebugLevel);
				MyfirstCarte.EntityGraphic(Scene);
			}
			
			
			
		}
		private void ISButtonShuffle_Click(UIButton sender)
		{
			Scene = Component_Scene.First;
			Array ValuesObjCollectifs = Enum.GetValues(typeof(Carte.ObjCollectifs));
			Array ValuesObjPrives = Enum.GetValues(typeof(Carte.ObjPrives));
			Array ValuesCommentaires = Enum.GetValues(typeof(Carte.Commentaires));
			if (Scene.Name == "TableJeu")
			{
				Carte MyfirstCarte = new Carte();
				MyfirstCarte.ObjectifCollectif = (Carte.ObjCollectifs)ValuesObjCollectifs.GetValue(rnd.Next(ValuesObjCollectifs.Length));
				MyfirstCarte.ObjectifPrive = (Carte.ObjPrives)ValuesObjPrives.GetValue(rnd.Next(ValuesObjPrives.Length));
				MyfirstCarte.Commentaire = (Carte.Commentaires)ValuesCommentaires.GetValue(rnd.Next(ValuesCommentaires.Length));
				MyfirstCarte.Valeur = rnd.Next(1,11);
				MyfirstCarte.Name = MyfirstCarte.ObjectifCollectif.ToString() + MyfirstCarte.ObjectifPrive.ToString() + MyfirstCarte.Commentaire.ToString() + MyfirstCarte.Valeur.ToString();
				MyfirstCarte.EntityGraphic(Scene);
			}
			
			
			
		}
		
		
		
		
		private void LanceJeuF()
		{
			Scene = Component_Scene.First;
			if(JeuMode == ModeJeu.NonLance)
			{
				//ScreenMessages.Add("Je vais lancer le jeu");
				Message.NvMess = DebugInfo.NiveauMessage.Debug;
				Message.DbgMess ="Je vais lancer le jeu";
				DebugInfo.Add(ModeAffichage, Message, DebugLevel);
				Component_MeshInSpace CarteMaitresse =(Component_MeshInSpace) Scene.GetComponent<Component_MeshInSpace>("Carte");
				CarteMaitresse.Enabled = false;
				NewGame = new GameML();
				int NbJoueur = 4;
				int CarteCachees = 3;
				int NbCarte = 5;
				NewGame.InitialisationJeu(NbJoueur, CarteCachees, NbCarte);
				JeuMode = ModeJeu.Initilisation;
			}
			else
			{
				//ScreenMessages.Add("Le jeu est deja lance");
				Message.NvMess = DebugInfo.NiveauMessage.Debug;
				Message.DbgMess ="Le jeu est deja lance";
				DebugInfo.Add(ModeAffichage, Message, DebugLevel);
			}
		}
		
		private void DistributionF()
		{
			if (JeuMode == ModeJeu.Initilisation)
			{
				NewGame.Distribution();
				int NbMain = NewGame.Joueurs[0].Jeu.Count();
				Message.NvMess = DebugInfo.NiveauMessage.Debug;
				Message.DbgMess ="Creer la main du joueur de  " + NbMain+" cartes";
				DebugInfo.Add(ModeAffichage, Message, DebugLevel);
				CreerCarteMain(NbMain);
				JeuMode = ModeJeu.Commentaire;
				EmissionCommentaireF();
			}
		}
		
		private void EmissionCommentaireF()
		{
			if (JeuMode == ModeJeu.Commentaire)
			{
				NewGame.EmissionCommentaire();
				//UIControl CommentairePan = (UIControl)GetComponent<UIControl>("CommentairePan");
				CommentairePan.Enabled = true;
				//JeuMode = ModeJeu.Commentaire;
			};
		}
		
		private void TirageCarteObjectifF()
		{
			bool TourCommentaire = VerifEmissionCommentaire();
			if (TourCommentaire && JeuMode == ModeJeu.Commentaire)
			{
				CommentairePan.Enabled = false;
				NewGame.Confrontation();
				Message.NvMess = DebugInfo.NiveauMessage.Debug;
				Message.DbgMess ="Tirage de la carte objectif";
				DebugInfo.Add(ModeAffichage, Message, DebugLevel);
				Message.NvMess = DebugInfo.NiveauMessage.Debug;
				Message.DbgMess ="La carte objectif est "+NewGame.CarteObjectif.Name;
				DebugInfo.Add(ModeAffichage, Message, DebugLevel);
				JeuMode = ModeJeu.Confrontation;
				MisePan.Enabled = true;
				MiseCachePan.Enabled = false;
				CarteObjectifCollectif = (Component_MeshInSpace)Scene.GetComponent<Component_MeshInSpace>("CarteObjectifCollectif").Clone();
				Scene.AddComponent(CarteObjectifCollectif);
				CarteObjectifCollectif.Name = "CarteObjectifCollectif2";
				BceCarteIngry.Add(CarteObjectifCollectif);
				NewGame.CarteObjectif.PrendreForme(CarteObjectifCollectif.Name,Scene);
				
			}
			else if(JeuMode == ModeJeu.Confrontation)
			{
				Message.NvMess = DebugInfo.NiveauMessage.Debug;
				Message.DbgMess ="La carte objectif est "+NewGame.CarteObjectif.Name;
				DebugInfo.Add(ModeAffichage, Message, DebugLevel);
				MisePan.Enabled = true;
				MiseCachePan.Enabled = false;
			}
		}
		
		private void ISButtonLanceJeu_Click(UIButton sender)
		{
			LanceJeuF();
			
		}
		
		private void ISButtonDistribution_Click(UIButton sender)
		{
			DistributionF();
		}
		
		
		private void ISButtonCommentaire_Click(UIButton sender)
		{
			EmissionCommentaireF();
		}
		
		private void ISButtonEmettreCommentaire_Click(UIButton sender)
		{
			Message.NvMess = DebugInfo.NiveauMessage.Debug;
			Message.DbgMess =sender.Text;
			DebugInfo.Add(ModeAffichage, Message, DebugLevel);
			//JeuMode = ModeJeu.CommentaireTermine;
			for (int f = 0; f < NewGame.Joueurs.Count;f++)
			{
				NewGame.automate(GameML.OrdreAutomate.Commentaire, NewGame.Joueurs[f]);
				//NewGame.Joueurs[f].EmissionComment= true;
			}
			NewGame.Joueurs[0].EmissionComment = true;
			TirageCarteObjectifF();
		}
		
		private bool VerifEmissionCommentaire()
		{
			int ok=0;
			for (int f = 0; f < NewGame.Joueurs.Count;f++)
			{
				if (NewGame.Joueurs[f].EmissionComment)
				{
					ok++;
				}
				else if (NewGame.Joueurs[f].statut == GameML.statutJoueur.Banque)
				{
					ok++;
				}
			}
			if (ok==NewGame.Joueurs.Count)
			{
				
				Message.DbgMess ="VerifEmissionCommentaire "+ok +" =" + true;
				DebugInfo.Add(ModeAffichage, Message, DebugLevel);
				Message.NvMess = DebugInfo.NiveauMessage.Debug;
				return true;
			}
			else
			{
				Message.DbgMess ="VerifEmissionCommentaire "+ok +" =" + false;
				DebugInfo.Add(ModeAffichage, Message, DebugLevel);
				Message.NvMess = DebugInfo.NiveauMessage.Debug;
				return false;
			}
		}
		
		private void ISButtonTirageCarteObjectif_Click(UIButton sender)
		{
			TirageCarteObjectifF();
			
		}
		private void ISButtonPasserModeAvancerCarte_Click(UIButton sender)
		{
			if(JeuMode == ModeJeu.Confrontation && true)
			{
				JeuMode = ModeJeu.ConfrontationCachee;
				MisePan.Enabled = false;
				MiseCachePan.Enabled = true;
				
			}
		}
		
		private void ISButtonMiser_Click(UIButton sender)
		{
			if(JeuMode == ModeJeu.ConfrontationCachee)
			{
				if(NewGame.VerifSelection(NewGame.Joueurs[0]) && NewGame.Joueurs[0].NbSelectionRetourne() == NewGame.CarteCachees && NewGame.Joueurs[0].ValeurSelection() >= NewGame.CarteObjectif.Valeur && NewGame.Joueurs[0].NbSelectionMinimum(NewGame.CarteObjectif.Valeur))
				{
					Message.NvMess = DebugInfo.NiveauMessage.Debug;
					Message.DbgMess = "La mise peut etre constituee";
					DebugInfo.Add(ModeAffichage, Message, DebugLevel);
					for (int i = 0;i < NewGame.Joueurs.Count ;i++)
					{
						NewGame.automate(GameML.OrdreAutomate.Mise, NewGame.Joueurs[i]);
					}
					JeuMode = ModeJeu.MiseTapis;
					MisePan.Enabled = false;
					MiseCachePan.Enabled = false;
					ResolutionF();
					
				}
				else if (NewGame.Joueurs[0].NbSelectionRetourne() != NewGame.CarteCachees)
				{
					Message.NvMess = DebugInfo.NiveauMessage.Debug;
					Message.DbgMess = "Vous ne pouvez pas miser, il y a un probleme avec les cartes carte cachees " + NewGame.Joueurs[0].NbSelectionRetourne() +"/" + NewGame.CarteCachees +" !! ";
					DebugInfo.Add(ModeAffichage, Message, DebugLevel);
				}
				else if (! NewGame.Joueurs[0].NbSelectionMinimum(NewGame.CarteObjectif.Valeur))
				{
					Message.NvMess = DebugInfo.NiveauMessage.Debug;
					Message.DbgMess = "Vous ne pouvez pas miser, il y a un probleme avec les cartes Non retournees!! le min est " + NewGame.Joueurs[0].NbSelectionMinimum(NewGame.CarteObjectif.Valeur);
					DebugInfo.Add(ModeAffichage, Message, DebugLevel);
				}
				else if( NewGame.Joueurs[0].ValeurSelection() < NewGame.CarteObjectif.Valeur)
				{
					Message.NvMess = DebugInfo.NiveauMessage.Debug;
					Message.DbgMess = "Vous ne pouvez pas miser, il y a un pobleme avec La valeur des cartes"+ NewGame.Joueurs[0].ValeurSelection() +"/"+ NewGame.CarteObjectif.Valeur+ "!!" ;
					DebugInfo.Add(ModeAffichage, Message, DebugLevel);
					
				}
				else
				{
					Message.NvMess = DebugInfo.NiveauMessage.Debug;
					Message.DbgMess = "Vous ne pouvez pas miser, il y a un pobleme";
					DebugInfo.Add(ModeAffichage, Message, DebugLevel);
				}
			}
			
			
		}
		
		private void ISButtonRevenirModeAvancerCarte_Click(UIButton sender)
		{
			if(JeuMode == ModeJeu.Confrontation)
			{
				JeuMode = ModeJeu.Confrontation;
				MisePan.Enabled = true;
				MiseCachePan.Enabled = false;
			}
			else if(JeuMode == ModeJeu.ConfrontationCachee && true)
			{
				JeuMode = ModeJeu.Confrontation;
				MiseCachePan.Enabled = false;
				MisePan.Enabled = true;
				
			}
		}
		
		private void ISButtonReLanceJeu_Click(UIButton sender)
		{
			if(JeuMode != ModeJeu.NonLance)
			{
				
				MisePan.Enabled = false;
				MiseCachePan.Enabled = false;
				foreach (Component_MeshInSpace os in BceCarteIngry)
				{
					os.Dispose();
				}
				NewGame.Dispose();
				Component_MeshInSpace cartobj = Scene.GetComponent<Component_MeshInSpace>("CarteObjectifCollectif");
				Component_MeshInSpace CarteMaitresse =(Component_MeshInSpace) Scene.GetComponent<Component_MeshInSpace>("Carte");
				cartobj.Enabled = false;
				CarteMaitresse.Enabled = false;
				NewGame = new GameML();
				int NbJoueur = 4;
				int CarteCachees = 3;
				int NbCarte = 5;
				NewGame.InitialisationJeu(NbJoueur, CarteCachees, NbCarte);
				JeuMode = ModeJeu.Initilisation;
				Message.NvMess = DebugInfo.NiveauMessage.Debug;
				Message.DbgMess = "Le jeu a redemarrer";
				DebugInfo.Add(ModeAffichage, Message, DebugLevel);
			}
		}
		
		private void ResolutionF()
		{
			if(JeuMode ==  ModeJeu.MiseTapis)
			{
				MisePan.Enabled = false;
				MiseCachePan.Enabled = false;
				MisePan.Enabled = false;
				MiseCachePan.Enabled = false;
				Message.DbgMess ="Le mode Resolution est maintenant actif";
				MisePan.Enabled = false;
				MiseCachePan.Enabled = false;
				Message.NvMess = DebugInfo.NiveauMessage.Debug;
				DebugInfo.Add(ModeAffichage, Message, DebugLevel);
				NewGame.CreerMiseJoueur(NewGame.Joueurs[0]);
				JeuMode = ModeJeu.Resolution;
				MisePan.Enabled = false;
				MiseCachePan.Enabled = true;
				NewGame.Resolution();
				JeuMode = ModeJeu.Attribution;
				AttributionF();
				
				
			}
			else
			{
				Message.DbgMess ="Le mode Resolution ne peut pas etre active";
				MisePan.Enabled = false;
				MiseCachePan.Enabled = false;
				Message.NvMess = DebugInfo.NiveauMessage.Debug;
				DebugInfo.Add(ModeAffichage, Message, DebugLevel);
			}
		}
		
		
		private void AttributionF()
		{
			if(JeuMode == ModeJeu.Attribution)
			{
				Message.DbgMess ="Je vais recreer la main du " + NewGame.Joueurs[0].Nom;
				MisePan.Enabled = false;
				MiseCachePan.Enabled = false;
				ResetGame();
				//NewGame.Dispose();
				//NewGame = new GameML();
				Message.NvMess = DebugInfo.NiveauMessage.Debug;
				DebugInfo.Add(ModeAffichage, Message, DebugLevel);
				int NlleMain = NewGame.Joueurs[0].Jeu.Count;
				CreerCarteMain(NlleMain);
				LeveCarteList = new List<Component_RigidBody>();
				JeuMode = ModeJeu.Commentaire;
				
			}
			else
			{
				Message.DbgMess ="Le mode Attribution ne peut pas etre active";
				Message.NvMess = DebugInfo.NiveauMessage.Debug;
				DebugInfo.Add(ModeAffichage, Message, DebugLevel);
			}
		}
		
		private void DemAutonomeF()
		{
			Message.DbgMess ="Demarrage Autonome : Les differentes phases seront gerees ici";
			Message.NvMess = DebugInfo.NiveauMessage.Debug;
			DebugInfo.Add(ModeAffichage, Message, DebugLevel);
			if(JeuMode == ModeJeu.NonLance)
			{
				LanceJeuF();
				DistributionF();
				//while(JeuMode == ModeJeu.)
				//{
				//	EmissionCommentaireF();
				//}
				//TirageCarteObjectifF();
				//ResolutionF();
				//AttributionF();
				
			}
			
		}
		
		private void ISButtonResolution_Click(UIButton sender)
		{
			ResolutionF();
		}
		
		
		private void ISButtonAttribution_Click(UIButton sender)
		{
			AttributionF();
		}
		private void ISButtonDemAutonome_Click(UIButton sender)
		{
			DemAutonomeF();
		}
		
		
		public List<Component_MeshInSpace> CreerCarteMain(int nbCr)
		{
			Scene = Component_Scene.First;
			List<Component_MeshInSpace> Main = new List<Component_MeshInSpace>();
			Component_MeshInSpace FormeGraphique = (Component_MeshInSpace) Scene.GetComponent<Component_MeshInSpace>("CarteModelMain");
			float decalageX = 5f/ Convert.ToSingle(nbCr);
			float decalageZ = 0.0001f;
			for (int t = 0; t < nbCr;t++)
			{
				Main.Add((Component_MeshInSpace)FormeGraphique.Clone());
				Main.Last().Enabled = true;
				if (Main.Last().Name!= null)
				{
					Component_RigidBody Rb= (Component_RigidBody) Main.Last().GetComponent<Component_RigidBody>("Rigid Body");
					Transform posT = Rb.Transform;
					float X = Convert.ToSingle(posT.Position.X);
					float Y = Convert.ToSingle(posT.Position.Y);
					float Z = Convert.ToSingle(posT.Position.Z);
					//Vector3 Mouv = new Vector3(posT.Position.X + t*(LongueurMain / nbCr), posT.Position.Y, posT.Position.Z + 0.001);
					Vector3 Mouv = new Vector3(X +decalageX*t,Y,Z+decalageZ*t);
					Rb.SetPosition(Mouv);
					Transform posTY = Rb.Transform;
					Vector3 MouvY = new Vector3(posTY.Position.X,posT.Position.Y,posTY.Position.Z);
					Rb.SetPosition(MouvY);
					Scene.AddComponent(Main.Last());
					Main.Last().Enabled = true;
					Main.Last().Name = "Name"+t;
					Message.NvMess = DebugInfo.NiveauMessage.Debug;
					Message.DbgMess = "Creer la carte  " +t+"/"+nbCr+ " de la main du joueur a la position " + Rb.Transform.ToString() + " Mouv= "+Mouv.ToString();
					DebugInfo.Add(ModeAffichage, Message, DebugLevel);
					List<Carte> JeuJoueur = NewGame.Joueurs[0].Jeu;
					if(JeuJoueur!= null)
					{
						
						Carte Gladit = (Carte) JeuJoueur[t];
						Gladit.PrendreForme(Main.Last().Name, Scene);
						Main.Last().Name = Gladit.Name;
						Message.NvMess = DebugInfo.NiveauMessage.Debug;
						Message.DbgMess = "La carte qui veut prendre forme est "+ Gladit.Name;
						DebugInfo.Add(ModeAffichage, Message, DebugLevel);
						/*Main.Last().AddComponent(Gladit);
						Message.NvMess = DebugInfo.NiveauMessage.Debug;
						Message.DbgMess = "L'ame de la carte est "+Gladit.Name;
						DebugInfo.Add(ModeAffichage, Message, DebugLevel);*/
						BceCarteIngry.Add(Main.Last());
					}
				}
			}
			return Main;
		}
		
		public void PousseCarte(Component_RigidBody CarteAdeplaceR)
		{
			Component_MeshInSpace CarteModelMise = Scene.GetComponent<Component_MeshInSpace>("CarteModelMise");
			Component_MeshInSpace CarteModelMain = Scene.GetComponent<Component_MeshInSpace>("CarteModelMain");
			Carte Ame = (Carte) NewGame.TrouverCarte(CarteAdeplaceR.Parent.Name);
			Message.DbgMess = "####Le nom de Carte est "+CarteAdeplaceR.Parent.Name;
			DebugInfo.Add(ModeAffichage, Message, DebugLevel);
			DebugInfo.Add(ModeAffichage, Message, DebugLevel);
			Message.DbgMess = "####L'ame de la Carte est "+Ame.Name+" son detenteur est "+ Ame.detenteur;
			DebugInfo.Add(ModeAffichage, Message, DebugLevel);
			DebugInfo.Add(ModeAffichage, Message, DebugLevel);
			if(Math.Round(CarteAdeplaceR.TransformV.Position.Y,5) == Math.Round(CarteModelMain.TransformV.Position.Y,5))
			{
				Vector3 PosAct = CarteAdeplaceR.TransformV.Position;
				Vector3 PosNouv = new Vector3(CarteAdeplaceR.TransformV.Position.X, CarteModelMise.TransformV.Position.Y, CarteModelMise.TransformV.Position.Z);
				CarteAdeplaceR.SetPosition(PosNouv);
				Ame.Selection = true;
				string message = "Bouge la carte de la main du joueur a la position " + CarteAdeplaceR.Transform.ToString() + " Mouv= "+PosNouv.ToString();
				ScreenMessages.Add(message);
				Log.Info(message);
			}
			else if(CarteAdeplaceR.TransformV.Position.Y == CarteModelMise.TransformV.Position.Y)
			{
				Vector3 PosAct = CarteAdeplaceR.TransformV.Position;
				Vector3 PosNouv = new Vector3(CarteAdeplaceR.TransformV.Position.X, CarteModelMain.TransformV.Position.Y, CarteAdeplaceR.TransformV.Position.Z);
				CarteAdeplaceR.SetPosition(PosNouv);
				CarteAdeplaceR.SetPosition(PosNouv);
				Ame.Selection = false;
				Message.NvMess = DebugInfo.NiveauMessage.Debug;
				Message.DbgMess = "Bouge la carte de la main du joueur a la position " + CarteAdeplaceR.Transform.ToString() + " Mouv= "+PosNouv.ToString();
				DebugInfo.Add(ModeAffichage, Message, DebugLevel);
			}
		}
		
		public void RetourneCarte(Component_RigidBody CarteAfairetourneR)
		{
			Carte Ame = (Carte) NewGame.TrouverCarte(CarteAfairetourneR.Parent.Name);
			Message.DbgMess = "####L'ame de la Carte est "+Ame.Name+" son detenteur est "+ Ame.detenteur;
			DebugInfo.Add(ModeAffichage, Message, DebugLevel);
			DebugInfo.Add(ModeAffichage, Message, DebugLevel);
			Angles NouvelAngle = new Angles(CarteAfairetourneR.TransformV.Rotation.Angles.Roll+180,CarteAfairetourneR.TransformV.Rotation.Angles.Pitch , CarteAfairetourneR.TransformV.Rotation.Angles.Yaw);
			CarteAfairetourneR.SetRotation(NouvelAngle);
			if (Ame.Retourne == true)
			{
				Ame.Retourne = false;
			}
			else if(Ame.Retourne == false)
			{
				Ame.Retourne = true;
			}
			Message.DbgMess = "Retourne la carte de la main du joueur a la position " + CarteAfairetourneR.Transform.ToString() + " Mouv= "+NouvelAngle.ToString();
			Message.NvMess = DebugInfo.NiveauMessage.Debug;
			DebugInfo.Add(ModeAffichage, Message, DebugLevel);
		}
		
		public void LeveCarte(Component_RigidBody CarteAdeplaceR)
		{
			Component_MeshInSpace CarteModelMise = Scene.GetComponent<Component_MeshInSpace>("CarteModelMise");
			Component_MeshInSpace CarteModelMain = Scene.GetComponent<Component_MeshInSpace>("CarteModelMain");
			Carte Ame = (Carte) NewGame.TrouverCarte(CarteAdeplaceR.Parent.Name);
			Message.DbgMess = "####Le nom de Carte est "+CarteAdeplaceR.Parent.Name;
			DebugInfo.Add(ModeAffichage, Message, DebugLevel);
			DebugInfo.Add(ModeAffichage, Message, DebugLevel);
			Message.DbgMess = "####L'ame de la Carte est "+Ame.Name+" son detenteur est "+ Ame.detenteur;
			DebugInfo.Add(ModeAffichage, Message, DebugLevel);
			DebugInfo.Add(ModeAffichage, Message, DebugLevel);
			if (LeveCarteList.Count > 0)
			{
				Component_RigidBody ExCarteAdeplaceR = LeveCarteList.Last();
				Vector3 ExPosAct = ExCarteAdeplaceR.TransformV.Position;
				Vector3 ExPosNouv = new Vector3(ExCarteAdeplaceR.TransformV.Position.X, ExCarteAdeplaceR.TransformV.Position.Y, ExCarteAdeplaceR.TransformV.Position.Z - 0.1f);
				if (ExCarteAdeplaceR.TransformV.Position.Z > CarteModelMise.TransformV.Position.Z)
				{
					ExCarteAdeplaceR.SetPosition(ExPosNouv);
					string message1 = "Bouge l'ancienne carte de la main du joueur a la position " + CarteAdeplaceR.Transform.ToString() + " Mouv= " + ExPosNouv.ToString();
					ScreenMessages.Add(message1);
					Log.Info(message1);
				}
			}
			if(LeveCarteList.Count > 0 && CarteAdeplaceR.Parent.Name != LeveCarteList.Last().Parent.Name)
			{
				Vector3 PosAct = CarteAdeplaceR.TransformV.Position;
				Vector3 PosNouv = new Vector3(CarteAdeplaceR.TransformV.Position.X, CarteAdeplaceR.TransformV.Position.Y, CarteAdeplaceR.TransformV.Position.Z + 0.1f);
				LeveCarteList.Add(CarteAdeplaceR);
				CarteAdeplaceR.SetPosition(PosNouv);
				Ame.Selection = true;
				string message = "Bouge la carte de la main du joueur a la position " + CarteAdeplaceR.Transform.ToString() + " Mouv= " + PosNouv.ToString();
				ScreenMessages.Add(message);
				Log.Info(message);
			}
			else if( LeveCarteList.Count <= 0)
			{
				Vector3 PosAct = CarteAdeplaceR.TransformV.Position;
				Vector3 PosNouv = new Vector3(CarteAdeplaceR.TransformV.Position.X, CarteAdeplaceR.TransformV.Position.Y, CarteAdeplaceR.TransformV.Position.Z + 0.1f);
				LeveCarteList.Add(CarteAdeplaceR);
				CarteAdeplaceR.SetPosition(PosNouv);
				Ame.Selection = true;
				string message = "Bouge la carte de la main du joueur a la position " + CarteAdeplaceR.Transform.ToString() + " Mouv= " + PosNouv.ToString();
				ScreenMessages.Add(message);
				Log.Info(message);
			}
		}
		//public void _MouseDoubleClick(NeoAxis.UIControl sender, NeoAxis.EMouseButtons button, ref bool handled)
        //{
        //}
        public void ResetGame()
        {
        	foreach (Component_MeshInSpace os in BceCarteIngry)
				{
					Carte regladit = os.GetComponent<Carte>(os.Name);
					Message.DbgMess = os.Name + "<- Cette cartemesh sera detruite ";
					Message.NvMess = DebugInfo.NiveauMessage.Debug;
					DebugInfo.Add(ModeAffichage, Message, DebugLevel);
					/*if (regladit.Name == os.Name)
					{
						Message.DbgMess = regladit.Name + "<- Cette carteame sera retiree ";
						Message.NvMess = DebugInfo.NiveauMessage.Debug;
						DebugInfo.Add(ModeAffichage, Message, DebugLevel);
						regladit.RemoveFromParent(true);
						Message.DbgMess = regladit.Name + "<- Cette carteame a ete retiree ";
						Message.NvMess = DebugInfo.NiveauMessage.Debug;
						DebugInfo.Add(ModeAffichage, Message, DebugLevel);//os.
						//os.re
					}*/
					os.Dispose();
				}
		}
		public void SourisScrut()//(NeoAxis.UIControl sender, NeoAxis.EMouseButtons button, ref bool handled)
		{
			Component_RigidBody ObjetSelectionneScene = (Component_RigidBody) Scene.GetComponent<Component_MeshInSpace>(ObjetSelectionne).GetComponent<Component_RigidBody>("Rigid Body");
			Component_MeshInSpace CarteModelMise = Scene.GetComponent<Component_MeshInSpace>("CarteModelMise");
			Component_MeshInSpace CarteModelMain = Scene.GetComponent<Component_MeshInSpace>("CarteModelMain");
			
			if(JeuMode == ModeJeu.Confrontation)
			{
				
				if (ObjetSelectionne != null)
				{
					Message.DbgMess = "SourisScrut Obj " +ObjetSelectionne +" "  + ObjetSelectionneScene.TransformV.Position.Y + " Mise " + CarteModelMise.TransformV.Position.Y + " Main " + CarteModelMain.TransformV.Position.Y;
					Message.NvMess = DebugInfo.NiveauMessage.Debug;
					DebugInfo.Add(ModeAffichage, Message, DebugLevel);
					if (ObjetSelectionneScene.TransformV.Position.Y == CarteModelMise.TransformV.Position.Y || ObjetSelectionneScene.TransformV.Position.Y == CarteModelMain.TransformV.Position.Y)
					{
						Message.DbgMess = "SourisScrut pousse Obj "+ObjetSelectionne +" "  + ObjetSelectionneScene.TransformV.Position.Y + " Mise " + CarteModelMise.TransformV.Position.Y + " Main " + CarteModelMain.TransformV.Position.Y;
						Message.NvMess = DebugInfo.NiveauMessage.Debug;
						DebugInfo.Add(ModeAffichage, Message, DebugLevel);
						PousseCarte(ObjetSelectionneScene);
					}
				}
			}
			else if(JeuMode == ModeJeu.ConfrontationCachee)
			{
				if (ObjetSelectionne != null)
				{
					Message.DbgMess = "SourisScrut Obj "+ObjetSelectionne +" " +ObjetSelectionneScene.TransformV.Position.Y + " Mise " + CarteModelMise.TransformV.Position.Y + " Main " + CarteModelMain.TransformV.Position.Y;
					Message.NvMess = DebugInfo.NiveauMessage.Debug;
					DebugInfo.Add(ModeAffichage, Message, DebugLevel);
					if (ObjetSelectionneScene.TransformV.Position.Y == CarteModelMise.TransformV.Position.Y)
					{
						Message.DbgMess = "SourisScrut retourne Obj "+ObjetSelectionne +" " + ObjetSelectionneScene.TransformV.Position.Y + " Mise " + CarteModelMise.TransformV.Position.Y + " Main " + CarteModelMain.TransformV.Position.Y;
						Message.NvMess = DebugInfo.NiveauMessage.Debug;
						DebugInfo.Add(ModeAffichage, Message, DebugLevel);
						RetourneCarte(ObjetSelectionneScene);
					}
				}
			}
			else
			{
				Message.DbgMess = "SourisScrut releve Obj "+ObjetSelectionne +" " + ObjetSelectionneScene.TransformV.Position.Y + " Mise " + CarteModelMise.TransformV.Position.Y + " Main " + CarteModelMain.TransformV.Position.Y;
				Message.NvMess = DebugInfo.NiveauMessage.Debug;
				DebugInfo.Add(ModeAffichage, Message, DebugLevel);
				LeveCarte(ObjetSelectionneScene);
			}
			Message.DbgMess = "J'ai doubleClicker";
			Message.NvMess = DebugInfo.NiveauMessage.Debug;
			DebugInfo.Add(ModeAffichage, Message, DebugLevel);
			
		}
		
		void InitializeSceneEvents()
		{
			// Subscribe to Render event of the scene.
			Component_Scene.First.RenderEvent += SceneRenderEvent;
		}
		
		void SceneRenderEvent(Component_Scene scene, Viewport viewport)
		{
			// Find object by the cursor. 
			var obj = GetObjectByCursor(viewport);
			
			// Draw selection border.
			if (obj != null)
			{
				viewport.Simple3DRenderer.SetColor(new ColorValue(1, 1, 0));
				viewport.Simple3DRenderer.AddBounds(obj.SpaceBounds.CalculatedBoundingBox);
				//ScreenMessages.Add(obj.Name);
				//Log.Info(obj.Name);
				ObjetSelectionne = obj.Name;
			}
		}
		List<Component_ObjectInSpace> GetObjectsThanCanBeSelected()
		{
			var result = new List<Component_ObjectInSpace>();
			foreach (var obj in Component_Scene.First.GetComponents<Component_MeshInSpace>())
			{
				// Skip ground.
				if (obj.Name != "Ground")
					result.Add(obj);
			}
			return result;
		}
		
		Component_ObjectInSpace GetObjectByCursor(Viewport viewport)
		{
			var mouse = MousePosition;
			if( touchPosition != null )
				mouse = touchPosition.Value;

			// Get scene object.
			var scene = Component_Scene.First;

			// Get world ray by cursor position.
			var ray = viewport.CameraSettings.GetRayByScreenCoordinates(mouse);

			// Get objects by the ray.
			var item = new Component_Scene.GetObjectsInSpaceItem(Component_Scene.GetObjectsInSpaceItem.CastTypeEnum.All, null, true, ray);
			scene.GetObjectsInSpace(item);

			// To test by physical objects:
			//scene.PhysicsRayTest()
			//scene.PhysicsContactTest()
			//scene.PhysicsConvexSweepTest()

			var objectsThanCanBeSelected = GetObjectsThanCanBeSelected();

			// Process objects.
			foreach (var resultItem in item.Result)
			{
				if (objectsThanCanBeSelected.Contains(resultItem.Object))
				{
					// Found.
					return resultItem.Object;
				}
			}

			return null;
		}
		
		
		
		
		private void lancement()
		{
			//base.OnEnabledInSimulation();
			Message.DbgMess = "Demarrage Main ";
			Message.NvMess = DebugInfo.NiveauMessage.Debug;
			DebugInfo.Add(ModeAffichage, Message, DebugLevel);
			Scene = Component_Scene.First;
			UIControl Main = (UIControl)GetComponent<UIControl>("Main");
			((UIButton)Main.GetComponent<UIButton>("Habillage")).Click += ISButtonTake_Click;
			//ScreenMessages.Add("Demarrage Main 1");
			((UIButton)Main.GetComponent<UIButton>("Shuffle")).Click += ISButtonShuffle_Click;
			//ScreenMessages.Add("Demarrage Main 2");
			((UIButton)Main.GetComponent<UIButton>("Demarrer")).Click += ISButtonLanceJeu_Click;
			//ScreenMessages.Add("Demarrage Main 3");
			((UIButton)Main.GetComponent<UIButton>("Distribution")).Click += ISButtonDistribution_Click;
			//ScreenMessages.Add("Demarrage Main 4");
			((UIButton)Main.GetComponent<UIButton>("Commentaire")).Click += ISButtonCommentaire_Click;
			//ScreenMessages.Add("Demarrage Main 5");
			((UIButton)Main.GetComponent<UIButton>("TireCarteObjectif")).Click += ISButtonTirageCarteObjectif_Click;
			((UIButton)Main.GetComponent<UIButton>("Demarrer")).Click += ISButtonLanceJeu_Click;
			((UIButton)Main.GetComponent<UIButton>("Redemarrer")).Click += ISButtonReLanceJeu_Click;
			((UIButton)Main.GetComponent<UIButton>("Resolution")).Click += ISButtonResolution_Click;
			((UIButton)Main.GetComponent<UIButton>("Attribution")).Click += ISButtonAttribution_Click;
			((UIButton)Main.GetComponent<UIButton>("DemAutonome")).Click += ISButtonDemAutonome_Click;
			
			//Main.Enabled = true;
			Message.DbgMess = "Demarrage CommentairePan ";
			Message.NvMess = DebugInfo.NiveauMessage.Debug;
			DebugInfo.Add(ModeAffichage, Message, DebugLevel);
			CommentairePan = (UIControl)GetComponent<UIControl>("CommentairePan");
			((UIButton)CommentairePan.GetComponent<UIButton>("EmissionComm")).Click += ISButtonEmettreCommentaire_Click;
			((UIButton)CommentairePan.GetComponent<UIButton>("EmissionComm2")).Click += ISButtonEmettreCommentaire_Click;
			((UIButton)CommentairePan.GetComponent<UIButton>("EmissionComm3")).Click += ISButtonEmettreCommentaire_Click;
			((UIButton)CommentairePan.GetComponent<UIButton>("EmissionComm4")).Click += ISButtonEmettreCommentaire_Click;
			
			Message.DbgMess = "Demarrage MisePan ";
			Message.NvMess = DebugInfo.NiveauMessage.Debug;
			DebugInfo.Add(ModeAffichage, Message, DebugLevel);
			MisePan = (UIControl)GetComponent<UIControl>("MisePan");
			((UIButton)MisePan.GetComponent<UIButton>("Validation")).Click += ISButtonPasserModeAvancerCarte_Click;
			
			Message.DbgMess = "Demarrage MiseCachePan ";
			Message.NvMess = DebugInfo.NiveauMessage.Debug;
			DebugInfo.Add(ModeAffichage, Message, DebugLevel);
			MiseCachePan = (UIControl)GetComponent<UIControl>("MiseCachePan");
			((UIButton)MiseCachePan.GetComponent<UIButton>("Retour")).Click += ISButtonRevenirModeAvancerCarte_Click;
			((UIButton)MiseCachePan.GetComponent<UIButton>("Validation")).Click += ISButtonMiser_Click;
			//FontaineCarte();
			
			
			
		}
		private void FontaineCarte()
		{
			Scene = Component_Scene.First;
			cartobjFontaine = Scene.GetComponent<Component_MeshInSpace>("Pluie");
			
		}
		private void update_Fontaine()
		{
			double testfps = EngineApp.FPS;
            NT = EngineApp.EngineTime;
            if(NT> PT && testfps > 60)
			{
				//ScreenMessages.Add("Plus de cartes NT > PT"+EngineApp.EngineTime);
				PT = NT + 0.5;
				ScreenMessages.Add("Plus de cartes à "+testfps);
				goutte = (Component_MeshInSpace)cartobjFontaine.Clone();
				Scene.AddComponent(goutte);
				goutte.Enabled = true;
				goutte.Name = "goutte";
				Component_RigidBody rigid = goutte.GetComponent<Component_RigidBody>("Rigid Body");
				//rigid.ApplyForce(new Vector3(0,5,0), new Vector3(0,-1,0));
				//rigid.ap
				//goutte
				//Component_RigidBody rigid = goutte.GetComponent<Component_RigidBody>("Rigid Body");
				//rigid.MotionType = Component_RigidBody.MotionTypeEnum.Static;
				//ScreenMessages.Add(rigid.Name);
			}
			else
				if(NT==PT)
				{
				goutte = (Component_MeshInSpace)Scene.GetComponent<Component_MeshInSpace>("goutte");
				goutte.Dispose();
			}
			;
		}
		
		private void update_time()
		{
			double testfps = EngineApp.FPS;
            NT = EngineApp.EngineTime;
            if(NT> PT)
			{
				//ScreenMessages.Add("Plus de cartes NT > PT"+EngineApp.EngineTime);
				PT = NT + 1;
				ScreenMessages.Add("Le temps va avancer d'une seconde"+ TimeGame);
				TimeGame += 1;
			}
			/*else
				if(NT==PT)
				{
					ScreenMessages.Add("Le temps va avancer d'une seconde test"+ TimeGame);
				}
			;*/
		}
		
	}
}