using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using NeoAxis;
using NeoAxis.Editor;
using System.Linq;

namespace Project
{
		public class Joueur : NeoAxis.Component
	{
		public int id;
		public String Nom;
		public List<Carte> Jeu = new List<Carte>() ;
		//public bool SelectionPrete ;
		public GameML.statutJoueur statut;
		public GameML.statutControl statutControl;
		public Mise MiseJ = new Mise();
		public bool EmissionComment = false;
		DebugInfo DebugInfo = new DebugInfo();
		DebugInfo.DebugMessage Message = new DebugInfo.DebugMessage();
		DebugInfo.ModeAffichage ModeAffichage = DebugInfo.ModeAffichage.Virtuel;
		DebugInfo.NiveauMessage DebugLevel = DebugInfo.NiveauMessage.Debug;
		public void AddToJeu(Carte CarteToAdd)
		
		{
			Jeu.Add(CarteToAdd);
			CarteToAdd.detenteur = this.Nom;
		}
		public void DeleteToJeu(Carte CarteToDelete)
		{
			Jeu.Remove(CarteToDelete);
			CarteToDelete.detenteur = "";
		}
		public int NbSelectionRetourne()
		{
			int NbRetourne = 0;
			foreach (Carte cart in Jeu)
			{
				if (cart.Selection == true)
				{
					if (cart.Retourne == true)
					{
						NbRetourne++;
					}
				}
			}
			return NbRetourne;
		}
		public int NbSelectionNormal()
		{
			int NbRetourne = 0;
			foreach (Carte cart in this.Jeu)
			{
				if (cart.Selection == true && cart.Retourne == false)
				{
						NbRetourne++;
				}
			}
			return NbRetourne;
		}
		public bool NbSelectionMinimum(int but)
		{
			bool rep = false;
			int som = 0;
			int min = 0;
			Carte cart;
			int Nbs = NbSelectionNormal();
			for (int i = 0; i < this.Jeu.Count ;i++)
			{
				cart = this.Jeu[i];
				if (cart.Selection == true && cart.Retourne == false )
				{
					if (som <= but && cart.Valeur < but)
					{
						som = som + cart.Valeur;
						min++;
						Message.DbgMess = " Sortie1  le Nbs est " + Nbs + "et le min est " + min + "et la somme est " + som + "et la valeur " + cart.Valeur;
						Message.NvMess = DebugInfo.NiveauMessage.Debug;
						DebugInfo.Add(ModeAffichage, Message, DebugLevel);
			
					}
					else if (this.NbSelectionNormal() == 1 && cart.Valeur < but)
					{
						som = som + cart.Valeur;
						min++;
						Message.DbgMess = " Sortie2 le Nbs est " + Nbs + "et le min est " + min + "et la somme est " + som + "et la valeur " + cart.Valeur;
						Message.NvMess = DebugInfo.NiveauMessage.Debug;
						DebugInfo.Add(ModeAffichage, Message, DebugLevel);
					}
				}
				i++;
			}
			Message.DbgMess = " Sortie3 le Nbs est " + Nbs + "et le min est " + min + "et la somme est " + som + "et la valeur ";
			Message.NvMess = DebugInfo.NiveauMessage.Debug;
			DebugInfo.Add(ModeAffichage, Message, DebugLevel);
			if (Nbs == min+1)
			{
				rep = true;
			}
			//ScreenMessages.Add(" le Nbs est "+Nbs+ "et le min est "+min);
			return rep;
		}
		
		
		public int ValeurSelection()
		{
			int som = 0;
			foreach (Carte cart in this.Jeu)
			{
				if (cart.Selection == true && cart.Retourne == false)
				{
					som = som + cart.Valeur;
				}
			}
			return som;
		}
		
		
		public void RemiseZero()
		{
			MiseJ = new Mise();
		}
		
		public void NettoyageJeu()
		{
			Carte cart;
			for (int p = 0; p < Jeu.Count;p++ )
			{
				cart = Jeu[p];
				if (cart.Selection == true)
				{
					Message.DbgMess ="La carte est valeur= "+ cart.Valeur+" ObjPC= " + cart.ObjectifCollectif+" ObjPV= " + cart.ObjectifPrive+" Commentaire= " + cart.Commentaire+" est retiree du jeu de "+cart.detenteur;
					Message.NvMess = DebugInfo.NiveauMessage.Debug;
					DebugInfo.Add(ModeAffichage, Message, DebugLevel);
					Jeu.Remove(cart);
				}
			}
			VerifNettoyage();
		}
		private void VerifNettoyage()
		{
			Carte CartMise;
			Carte CartJeu;
			for (int i = 0; i < MiseJ.CarteCachee.Count;i++)
			{
				CartMise = MiseJ.CarteCachee[i];
				for (int z = 0; z < Jeu.Count;z++)
				{
					CartJeu = Jeu[z];
					if(CartMise.Valeur == CartJeu.Valeur && CartMise.Commentaire == CartJeu.Commentaire && CartMise.ObjectifCollectif == CartJeu.ObjectifCollectif && CartMise.ObjectifPrive == CartJeu.ObjectifPrive)
					{
						Jeu.Remove(CartJeu);
					}
				}
			}
			for (int i = 0; i < MiseJ.CarteMonnaie.Count;i++)
			{
				CartMise = MiseJ.CarteMonnaie[i];
				for (int z = 0; z < Jeu.Count;z++)
				{
					CartJeu = Jeu[z];
					if(CartMise.Valeur == CartJeu.Valeur && CartMise.Commentaire == CartJeu.Commentaire && CartMise.ObjectifCollectif == CartJeu.ObjectifCollectif && CartMise.ObjectifPrive == CartJeu.ObjectifPrive)
					{
						Jeu.Remove(CartJeu);
					}
				}
			}
		}
		
		public void CreerMise()
		{
			for (int i = 0; i < Jeu.Count;i++)
			{
				Carte cart = Jeu[i];
				if (cart.Selection == true && cart.Retourne == false )
				{
					MiseJ.AddToCarteMonnaie(cart);
					Jeu.Remove(cart);
					Message.DbgMess ="La carte "+cart.Name+"a ete tranferee dans la Mise du joueur "+Nom+" ";
					Message.NvMess = DebugInfo.NiveauMessage.Debug;
					DebugInfo.Add(ModeAffichage, Message, DebugLevel);
						
				}
				else if (cart.Selection == true && cart.Retourne == false)
				{
					MiseJ.AddToCarteCachee(cart);
					Jeu.Remove(cart);
					Message.DbgMess ="La carte cachee "+cart.Name+"a ete tranferee dans la Mise du joueur "+Nom+" ";
					Message.NvMess = DebugInfo.NiveauMessage.Debug;
					DebugInfo.Add(ModeAffichage, Message, DebugLevel);
				}
			}
			MiseJ.MisePrete = true;
			Message.DbgMess ="La Mise du joueur "+Nom+" est prete";
			Message.NvMess = DebugInfo.NiveauMessage.Debug;
			DebugInfo.Add(ModeAffichage, Message, DebugLevel);
				
			}
	
	
	
		
	}
}