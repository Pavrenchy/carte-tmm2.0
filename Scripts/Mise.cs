using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using NeoAxis;
using NeoAxis.Editor;
using System.Linq;

namespace Project
{
	public class Mise : NeoAxis.Component
	{
		public List<Carte> CarteMonnaie = new List<Carte>() ;
		public List<Carte> CarteCachee = new List<Carte>() ;
		public bool MisePrete = false;
		DebugInfo.DebugMessage Message = new DebugInfo.DebugMessage();
		DebugInfo.ModeAffichage ModeAffichage = DebugInfo.ModeAffichage.Virtuel;
		DebugInfo.NiveauMessage DebugLevel = DebugInfo.NiveauMessage.Debug;
		public void AddToCarteMonnaie(Carte CarteToAdd)
		{
			CarteMonnaie.Add(CarteToAdd);
		}
		public void DeleteToCarteMonnaie(Carte CarteToDelete)
		{
			CarteMonnaie.Remove(CarteToDelete);
		}
		public void AddToCarteCachee(Carte CarteToAdd)
		{
			CarteCachee.Add(CarteToAdd);
		}
		public void DeleteToCarteCachee(Carte CarteToDelete)
		{
			CarteCachee.Remove(CarteToDelete);
		}
		public int somme()
		{
			int som = 0;
			foreach (Carte lacarte in CarteMonnaie)
			{
				som += lacarte.Valeur;
			}
			return som;
		}
		public void MontreMise()
		{
			DebugInfo DebugInfo = new DebugInfo();
			foreach(Carte cart in CarteMonnaie)
			{
				Message.DbgMess = "Selection non cachee " + cart.Name;
				Message.NvMess = DebugInfo.NiveauMessage.Debug;
				DebugInfo.Add(ModeAffichage, Message, DebugLevel);
			}
			foreach(Carte cart in CarteCachee)
			{
				Message.DbgMess = "Selection  cachee " + cart.Name;
				Message.NvMess = DebugInfo.NiveauMessage.Debug;
				DebugInfo.Add(ModeAffichage, Message, DebugLevel);
			}
			
		}
	}
}