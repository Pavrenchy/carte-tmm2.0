using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using NeoAxis;
using NeoAxis.Editor;
using System.Linq;

namespace Project
{
	public class GameML : NeoAxis.Component
	{
		public List<Joueur> Joueurs = new List<Joueur>();
		public List<Mise> Mises = new List<Mise>();
		public List<Carte> Defausse = new List<Carte>();
		public Carte CarteObjectif = new Carte();
		static System.Random rnd = new System.Random();
		public int NbJoueur;
		public int CarteCachees;
		public int NbCarte;
		DebugInfo DebugInfo = new DebugInfo();
		DebugInfo.DebugMessage Message = new DebugInfo.DebugMessage();
		DebugInfo.ModeAffichage ModeAffichage = DebugInfo.ModeAffichage.Virtuel;
		DebugInfo.NiveauMessage DebugLevel = DebugInfo.NiveauMessage.Debug;
		
		public enum OrdreAutomate
		{
			Mise,
			Commentaire
		}
		
		public enum statutControl
		{
			Automate,
			Normal
		}
		
		public enum statutJoueur
		{
			Banque,
			Normal
		}

		public void InitialisationJeu(int NbJoueur, int CarteCachees, int NbCarte)
		{
			this.NbJoueur = NbJoueur;
			this.CarteCachees = CarteCachees;
			this.NbCarte = NbCarte;
			/*NbJoueur =4;
			CarteCachees=3;
			NbCarte=5;*/
			List<String> testNom = new List<string>() { "Juan", "Electron", "Samuel", "Robert" };
			Message.DbgMess ="Initilisation du jeu " + NbJoueur + " " + CarteCachees + " " + NbCarte;
			Message.NvMess = DebugInfo.NiveauMessage.Debug;
			DebugInfo.Add(ModeAffichage, Message, DebugLevel);
			Message.DbgMess ="Nommer les joueurs";
			Message.NvMess = DebugInfo.NiveauMessage.Debug;
			DebugInfo.Add(ModeAffichage, Message, DebugLevel);

			for (int i = 0; i < NbJoueur; i++)
			{
				Joueurs.Add(new Joueur());
				Joueurs.Last().Nom = testNom[i];
				Joueurs.Last().statutControl = statutControl.Automate;
				Joueurs.Last().statut = GameML.statutJoueur.Normal;
				Message.DbgMess =Joueurs.Last().Nom + " est entre dans le game";
				Message.NvMess = DebugInfo.NiveauMessage.Debug;
				DebugInfo.Add(ModeAffichage, Message, DebugLevel);
			}
			Joueurs[0].statutControl = statutControl.Normal; ////////Un jour a retirer 
			if (Joueurs.Count > 0)
			{
				int numBanque = rnd.Next(Joueurs.Count - 1);
				if (numBanque == 0) 
				{ 
					numBanque++;
				}
				Joueurs[numBanque].statut = GameML.statutJoueur.Banque;
				Message.DbgMess ="Attribution du role de " + Joueurs[numBanque].statut + " a " + Joueurs[numBanque].Nom;
				Message.NvMess = DebugInfo.NiveauMessage.Debug;
				DebugInfo.Add(ModeAffichage, Message, DebugLevel);

			}
			//VoirJeuJoueurs();

		}

		public List<Carte> reDistribution()
		{
			string Message1 = "La banque doit distribuer " + NbCarte+ " cartes  pour " + (Joueurs.Count-1)+" joueurs";
			Message.DbgMess ="La banque doit distribuer " + NbCarte+ " cartes  pour " + (Joueurs.Count-1)+" joueurs";
			Message.NvMess = DebugInfo.NiveauMessage.Debug;
			DebugInfo.Add(ModeAffichage, Message, DebugLevel);
			List<Carte> MiseTempo = new List<Carte>();
			Array ValuesObjCollectifs = Enum.GetValues(typeof(Carte.ObjCollectifs));
			Array ValuesObjPrives = Enum.GetValues(typeof(Carte.ObjPrives));
			Array ValuesCommentaires = Enum.GetValues(typeof(Carte.Commentaires));
			for (int u = 0; u < NbCarte*(Joueurs.Count-1);u++)
			{
				int CartetempoAlert = 0;
				while (CartetempoAlert == 0)
				{
					Carte CarteTempo = new Carte();
					CarteTempo.ObjectifCollectif = (Carte.ObjCollectifs)ValuesObjCollectifs.GetValue(rnd.Next(ValuesObjCollectifs.Length));
					CarteTempo.ObjectifPrive = (Carte.ObjPrives)ValuesObjPrives.GetValue(rnd.Next(ValuesObjPrives.Length));
					CarteTempo.Commentaire = (Carte.Commentaires)ValuesCommentaires.GetValue(rnd.Next(ValuesCommentaires.Length));
					CarteTempo.Valeur = rnd.Next(1,11);
					CarteTempo.Name = CarteTempo.ObjectifCollectif.ToString() + CarteTempo.ObjectifPrive.ToString() + CarteTempo.Commentaire.ToString() + CarteTempo.Valeur.ToString();
					Message.DbgMess ="Les cartes sont creees "+CarteTempo.Name;
					Message.NvMess = DebugInfo.NiveauMessage.Debug;
					DebugInfo.Add(ModeAffichage, Message, DebugLevel);
					if (!MiseTempo.Exists(h => h.Name==CarteTempo.Name))
					{
						int nouvelle = 0;
						for (int j = 0; j < Joueurs.Count; j++)
						{
							if (Joueurs[j].Jeu.Exists(h => h.Name==CarteTempo.Name))
							{
								nouvelle++;
							}
						}
						if (nouvelle == 0)
						{
							MiseTempo.Add(CarteTempo);
							CartetempoAlert++;
						}
					}
				}
			}
			return MiseTempo;
		}
		public void Distribution()
		{
		List<Carte> MiseTempo = reDistribution();
		if (Joueurs.Count > 0 && MiseTempo.Count > 0)
		{
			int z = 0;
			for (int j = 0; j < Joueurs.Count; j++)
			{			
				for (int x = 0; x < NbCarte; x++)
				{
					if( Joueurs[j].statut == GameML.statutJoueur.Normal)
					{
						Joueurs[j].AddToJeu(MiseTempo[z]);//j'ai failli devenir fou x -> z
						Message.DbgMess ="Le joueur "+j+" " + Joueurs[j].Nom + " recoit la carte " + MiseTempo[z].Name+" son jeu compte "+Joueurs[j].Jeu.Count()+" cartes";
						Message.NvMess = DebugInfo.NiveauMessage.Debug;
						DebugInfo.Add(ModeAffichage, Message, DebugLevel);
						//VoirJeuJoueurs();
						z++;
					}
				}
			}
		}
			
			Message.DbgMess ="Les joueurs ont recu "+MiseTempo.Count+ " cartes";
			Message.NvMess = DebugInfo.NiveauMessage.Debug;
			DebugInfo.Add(ModeAffichage, Message, DebugLevel);
			//VoirJeuJoueurs();
		}
		
		public bool VerifSelection(Joueur Jp)
		{
			bool rep = false;
			int NbRetourne = 0;
			int NbSelectionne = 0;
			foreach (Carte cart in Jp.Jeu)
			{
				if (cart.Selection == true)
				{
					NbSelectionne++;
					if (cart.Retourne == true)
					{
						NbRetourne++;
					}
				}
			}
			if (this.CarteCachees == NbRetourne && Jp.NbSelectionMinimum(this.CarteObjectif.Valeur))
			{
				rep= true;
			}
			else
			{
				rep = false;
			}
			return rep;
		}
		
		private bool VerifAvantMise (Joueur jp)
		{
			bool rep = false;
			if(this.VerifSelection(jp) && jp.NbSelectionRetourne() == this.CarteCachees && jp.ValeurSelection() >= this.CarteObjectif.Valeur && jp.NbSelectionMinimum(this.CarteObjectif.Valeur))
			{
				rep = true;
				Message.NvMess = DebugInfo.NiveauMessage.Debug;
				Message.DbgMess ="La mise est acceptable pour le joueur "+jp.Nom;
				DebugInfo.Add(ModeAffichage, Message, DebugLevel);
			}
			return rep;
		}
		
		public void CreerMiseJoueur(Joueur jp)
		{
			if (VerifAvantMise(jp))
			{
				jp.CreerMise();	
			}
		}
		
		public int SommeListeCart(List<Carte> maliste)
		{
			int k = 0;
			foreach(Carte cart in maliste)
			{
				k = k + cart.Valeur;
			}
			return k;
		}
		
		public List<Carte> TrouveLaMiseNonRetourne(Joueur jp, Carte CarteObjectif )
		{
			Message.DbgMess = "La methode 1 est utilisee pour trouver la carte monnaie" + jp.Nom;
			Message.NvMess = DebugInfo.NiveauMessage.Debug;
			DebugInfo.Add(ModeAffichage, Message, DebugLevel);
			List<Carte> miseminimal = new List<Carte>();
			for (int i = 0; i < jp.Jeu.Count;i++ )
			{
				List<Carte> jeureverse = jp.Jeu.OrderBy(p => p.Valeur).ToList();
				Carte cart = jeureverse[i];
				if (cart.Valeur >= CarteObjectif.Valeur && SommeListeCart(miseminimal) < CarteObjectif.Valeur)
				{
					miseminimal.Add(cart);
				}
			}
			
			if (SommeListeCart(miseminimal) < CarteObjectif.Valeur)
			{

				//######################## Implementer la recherche de la meilleure combinaison
				Message.DbgMess = "La methode 2 est utilisee pour trouver la carte monnaie" + jp.Nom;
				Message.NvMess = DebugInfo.NiveauMessage.Debug;
				DebugInfo.Add(ModeAffichage, Message, DebugLevel);
				List<Carte> jeureverse = jp.Jeu.OrderBy(p => p.Valeur).ToList();
				//jeureverse.Reverse();
				miseminimal = new List<Carte>();
				for (int i = 0; i < jeureverse.Count;i++ )
				{
					Carte cart = jeureverse[i];
					if (cart.Valeur <= CarteObjectif.Valeur && SommeListeCart(miseminimal) < CarteObjectif.Valeur)
					{
						miseminimal.Add(cart);
					}
					if (cart.Valeur <= CarteObjectif.Valeur && miseminimal.Count <0)
					{
						miseminimal.Add(cart);
					}
				}
			}
			if(miseminimal.Count == 0)
			{
				Message.DbgMess = "Impossible de trouver la carte monnaie" + jp.Nom;
				Message.NvMess = DebugInfo.NiveauMessage.Debug;
				DebugInfo.Add(ModeAffichage, Message, DebugLevel);
			}

			return miseminimal;
		}
		
		
		
		public List<Carte> TrouveLaMise(Joueur jp)
		{
			List<Carte> TestMiseCarte = TrouveLaMiseNonRetourne(jp ,this.CarteObjectif);
			return TestMiseCarte;
		}
		
		public void automate(OrdreAutomate Ordre, Joueur jp)
		{
			switch(Ordre)
			{
				case(OrdreAutomate.Commentaire):
				{
						if(jp.statutControl == statutControl.Automate && jp.statut != GameML.statutJoueur.Banque)
						{
							Message.DbgMess = "L'automate va emettre un commentaire pour" + jp.Nom;
							Message.NvMess = DebugInfo.NiveauMessage.Debug;
							DebugInfo.Add(ModeAffichage, Message, DebugLevel);
							jp.EmissionComment = true;
						}
						break;
				}
				
				case(OrdreAutomate.Mise):
				{
						if(jp.statutControl == statutControl.Automate)
						{
							Message.DbgMess = "L'automate va creer la mise pour " + jp.Nom;
							Message.NvMess = DebugInfo.NiveauMessage.Debug;
							DebugInfo.Add(ModeAffichage, Message, DebugLevel);
							Message.DbgMess ="Le jeu est pour " +jp.Nom+"*********" ;
							Message.NvMess = DebugInfo.NiveauMessage.Debug;
							DebugInfo.Add(ModeAffichage, Message, DebugLevel);
							foreach(Carte carto in jp.Jeu)
							{
								
								Message.DbgMess =carto.Name ;
								Message.NvMess = DebugInfo.NiveauMessage.Debug;
								DebugInfo.Add(ModeAffichage, Message, DebugLevel);	
							}
							Message.DbgMess ="*************************************" ;
							Message.NvMess = DebugInfo.NiveauMessage.Debug;
							DebugInfo.Add(ModeAffichage, Message, DebugLevel);
							
							if(this.CarteObjectif.Name != "")
							{
								List<Carte> CarteMonnaieAutomate = TrouveLaMise(jp);
								foreach(Carte cartmo in CarteMonnaieAutomate)
								{
									jp.MiseJ.AddToCarteMonnaie(cartmo);
									//jp.Jeu.Remove(cartmo);
									cartmo.Selection = true;
									cartmo.Retourne = false;
									Message.DbgMess ="La carte "+cartmo.Name+" a ete rajoute a cartemonnaie" ;
									Message.NvMess = DebugInfo.NiveauMessage.Debug;
									DebugInfo.Add(ModeAffichage, Message, DebugLevel);
								}
								int Nb = 0;
								foreach(Carte cart in jp.Jeu)
								{
									if (! CarteMonnaieAutomate.Exists(h => h.Name==cart.Name))
									{
										if(Nb < this.CarteCachees)
										{
											cart.Selection = true;
											cart.Retourne = true;
											jp.MiseJ.AddToCarteCachee(cart);
											//jp.Jeu.Remove(cart);
											Message.DbgMess ="La carte "+cart.Name+" a ete rajoute a "+Nb+" cartecachee" ;
											Message.NvMess = DebugInfo.NiveauMessage.Debug;
											DebugInfo.Add(ModeAffichage, Message, DebugLevel);
											Nb++;
										}
									}
								}
								if(VerifAvantMise(jp))
								{
									jp.MiseJ.MisePrete = true;
									Message.DbgMess ="La Mise est Ok pour " +jp.Nom+"##############" ;
									Message.NvMess = DebugInfo.NiveauMessage.Debug;
									DebugInfo.Add(ModeAffichage, Message, DebugLevel);
									jp.MiseJ.MontreMise();
									Message.DbgMess ="####################################" ;
									Message.NvMess = DebugInfo.NiveauMessage.Debug;
									DebugInfo.Add(ModeAffichage, Message, DebugLevel);
								}
								else
								{
									Message.DbgMess ="La Mise est Ko pour " +jp.Nom+"##############" ;
									Message.NvMess = DebugInfo.NiveauMessage.Debug;
									DebugInfo.Add(ModeAffichage, Message, DebugLevel);
									jp.MiseJ.MontreMise();
									Message.DbgMess ="####################################" ;
									Message.NvMess = DebugInfo.NiveauMessage.Debug;
									DebugInfo.Add(ModeAffichage, Message, DebugLevel);
									
								}
							}
							
							
							
							
						}
						break;


				}
				default:
				{
						Message.DbgMess ="L'automate ne reconnait pas cet ordre";
						Message.NvMess = DebugInfo.NiveauMessage.Debug;
						DebugInfo.Add(ModeAffichage, Message, DebugLevel);
						break;
				}
			}
		}
		
		
		public void EmissionCommentaire()
		{
			Message.DbgMess ="La banque ne peut pas emettre de commentaire";
			Message.NvMess = DebugInfo.NiveauMessage.Debug;
			DebugInfo.Add(ModeAffichage, Message, DebugLevel);
			Message.DbgMess ="Les joueurs commentent";
			Message.NvMess = DebugInfo.NiveauMessage.Debug;
			DebugInfo.Add(ModeAffichage, Message, DebugLevel);
			//VoirJeuJoueurs();
		}
		public void Confrontation()
		{
			Message.DbgMess ="La banque tire une carte";
			Message.NvMess = DebugInfo.NiveauMessage.Debug;
			DebugInfo.Add(ModeAffichage, Message, DebugLevel);
			List<Carte> MiseCarteTirage = reDistribution();
			Message.DbgMess ="La banque a tire "+MiseCarteTirage[0].Name;
			Message.NvMess = DebugInfo.NiveauMessage.Debug;
			DebugInfo.Add(ModeAffichage, Message, DebugLevel);
			CarteObjectif = MiseCarteTirage[0];
			Message.DbgMess ="Les joueurs proposent une mise pour gagner la carte";
			Message.NvMess = DebugInfo.NiveauMessage.Debug;
			DebugInfo.Add(ModeAffichage, Message, DebugLevel);
		}
		public void Resolution()
		{
			Message.DbgMess ="La banque ne fait rien";
			Message.NvMess = DebugInfo.NiveauMessage.Debug;
			DebugInfo.Add(ModeAffichage, Message, DebugLevel);
			Message.DbgMess ="Creation des mises";
			Message.NvMess = DebugInfo.NiveauMessage.Debug;
			DebugInfo.Add(ModeAffichage, Message, DebugLevel);
			int TestBceMise = 0;
			Message.DbgMess ="TestBceMise= "+TestBceMise;
			Message.NvMess = DebugInfo.NiveauMessage.Debug;
			DebugInfo.Add(ModeAffichage, Message, DebugLevel);
			for (int i = 0; i < Joueurs.Count; i++)
			{
				Joueur jp = Joueurs[i];
				if(! jp.MiseJ.MisePrete)
				{
					if (jp.statut == GameML.statutJoueur.Normal)
					{
						Message.DbgMess = "TestBceMise a augmente par " + jp.Nom;
						Message.NvMess = DebugInfo.NiveauMessage.Debug;
						DebugInfo.Add(ModeAffichage, Message, DebugLevel);
						TestBceMise = TestBceMise + 1;
					}
				}
				Message.DbgMess ="Boucle TestBceMise= "+TestBceMise;
				Message.NvMess = DebugInfo.NiveauMessage.Debug;
				DebugInfo.Add(ModeAffichage, Message, DebugLevel);
				
			}
			if (TestBceMise == 0)
			{
				Message.DbgMess ="Tout les mises sont bonnes, un des joueurs va tout emporter";
				Message.NvMess = DebugInfo.NiveauMessage.Debug;
				DebugInfo.Add(ModeAffichage, Message, DebugLevel);
				Tapis MonTapis = new Tapis();
				String NomVainqueur =MonTapis.AttributionMiseTapis(this);
				Message.DbgMess ="Le vainqueur est "+ NomVainqueur;
				Message.NvMess = DebugInfo.NiveauMessage.Debug;
				DebugInfo.Add(ModeAffichage, Message, DebugLevel);
			}
			else 
			{
				Message.DbgMess ="TestBceMise ="+TestBceMise;
				Message.NvMess = DebugInfo.NiveauMessage.Debug;
				DebugInfo.Add(ModeAffichage, Message, DebugLevel);
			}
			
		}
		public Carte TrouverCarte(string nom)
		{
			Carte trouve=new Carte();
			for (int j = 0; j < Joueurs.Count; j++)
			{
				if (Joueurs[j].Jeu.Exists(h => h.Name==nom))
				{
					trouve=Joueurs[j].Jeu.Find(h => h.Name==nom);
				}
			}
			return trouve;
		}
		
		public void VoirJeuJoueurs()
		{
			foreach (Joueur orejou in Joueurs)
			{
				Message.DbgMess = "Le jeu est pour " + orejou.Nom + "*********";
				Message.NvMess = DebugInfo.NiveauMessage.Debug;
				DebugInfo.Add(ModeAffichage, Message, DebugLevel);
				foreach (Carte carto in orejou.Jeu)
				{
					Message.DbgMess = carto.Name;
					Message.NvMess = DebugInfo.NiveauMessage.Debug;
					DebugInfo.Add(ModeAffichage, Message, DebugLevel);
				}
				Message.DbgMess = "*************************************";
				Message.NvMess = DebugInfo.NiveauMessage.Debug;
				DebugInfo.Add(ModeAffichage, Message, DebugLevel);
			}
		}
		private void Attribution()
		{
			Message.DbgMess ="Comparaison des mises";
			Message.NvMess = DebugInfo.NiveauMessage.Debug;
			for (int i = 0; i < Joueurs.Count;i++)
			{
				Joueur jp = Joueurs[i];
				jp.MiseJ.MisePrete = false;
			}
			DebugInfo.Add(ModeAffichage, Message, DebugLevel);
			Message.DbgMess ="Les joueurs recoivent les mises";
			Message.NvMess = DebugInfo.NiveauMessage.Debug;
			DebugInfo.Add(ModeAffichage, Message, DebugLevel);
		}
		
		
	}
	
	
}	
		