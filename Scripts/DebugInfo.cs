using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using NeoAxis;
using NeoAxis.Editor;

namespace Project
{
	public class DebugInfo : NeoAxis.Component
	{
		public enum ModeAffichage
		{
			Partout,
			Virtuel,
			EcranJeu,
			Console,
			Normal,
			Fichier,
			Aucun
		}
		public enum NiveauMessage
		{
			Normal,
			Debug,
			Aucun
		}

		public class DebugMessage
		{
			public NiveauMessage NvMess;
			public string DbgMess;
			
		}
		private string MontreMessage(DebugMessage message, NiveauMessage NvMessaSouhaite)
		{
			string reponse = message.DbgMess;
			if (message.NvMess == NvMessaSouhaite )
			{
				reponse = message.DbgMess;
				//ScreenMessages.Add("Qu'est ce qui se passee #####################################");
				//return reponse;

			}
			else if(NvMessaSouhaite == NiveauMessage.Debug)
			{
				reponse = message.DbgMess;
				//ScreenMessages.Add("Qu'est ce qui se passee *************************************");
			}
			else if(NvMessaSouhaite == NiveauMessage.Aucun)
			{
				reponse = "";
				//ScreenMessages.Add("Qu'est ce qui se passee --------------------------------------");
			}
			return reponse;
		}
		public void Add(ModeAffichage ModeAffich, DebugMessage message, NiveauMessage NvMessaSouhaite)
		{
			string txt = (string) MontreMessage(message,NvMessaSouhaite);
			switch(ModeAffich)
			{
				case(ModeAffichage.Virtuel):
				{
						
						ScreenMessages.Add(txt);
						Log.Info(txt);
						break;
				}
				case(ModeAffichage.Aucun):
				{
						
						break;
				}
				case(ModeAffichage.EcranJeu):
				{
						ScreenMessages.Add(txt);
						//Log.Info(txt);
						break;
				}
				case(ModeAffichage.Console):
				{
						//ScreenMessages.Add(txt);
						Log.Info(txt);
						break;
				}
				case(ModeAffichage.Normal):
				{
						//ScreenMessages.Add(txt);
						Log.Info(txt);
						break;
				}
				default:
				{
						
						break;
				}
			}
		}
	}
}