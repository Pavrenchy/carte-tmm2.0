using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using NeoAxis;
using NeoAxis.Editor;
using System.Linq;

namespace Project
{
	public class Tapis : NeoAxis.Component
	{
		Carte CarteObjectif;
		List<Mise> TapisMise= new List<Mise>();
		static System.Random rnd = new System.Random();
		
		private void DefinirCarteObjectif(GameML Game)
		{
				 CarteObjectif = Game.CarteObjectif;
		}
		
		private void SoumettreMise(Joueur donneur)
		{
			if (donneur.statut != GameML.statutJoueur.Banque)
			{
				TapisMise.Add(donneur.MiseJ);
			}
		}
		
		private void DeposerMise(Joueur donneur)
		{
			if (donneur.statut != GameML.statutJoueur.Banque)
			{
				TapisMise.Add(donneur.MiseJ);
				donneur.RemiseZero();
				donneur.NettoyageJeu();
			}
		}
		private Joueur ResolutionTapis(List<Joueur> gameinprogress )
		{
			Joueur VainqueurP = gameinprogress.First() ;
			foreach(Joueur lejoueur in gameinprogress)
			{
				if(lejoueur.statut != GameML.statutJoueur.Banque)
				{
					if ( lejoueur.MiseJ.somme() > VainqueurP.MiseJ.somme())
					{
						VainqueurP = lejoueur;
					}
					else if ( lejoueur.MiseJ.somme() == VainqueurP.MiseJ.somme())
					{
						List<Joueur> TireJoueur = new List<Joueur>() ;
						TireJoueur.Add(VainqueurP);
						TireJoueur.Add(lejoueur);
						VainqueurP = TireJoueur[rnd.Next(TireJoueur.Count)];
					} 
				}
			}
			return VainqueurP;
		}
		public void resetTapis()
		{
			TapisMise = new List<Mise>(); 
		}
		public string AttributionMiseTapis(GameML game)
		{
			CarteObjectif = game.CarteObjectif;
			List<Joueur> gameinprogress = game.Joueurs;
			Joueur Vainqueur = ResolutionTapis(gameinprogress);
			foreach(Joueur lejoueur in gameinprogress)
			{
				DeposerMise(lejoueur);
			}
			foreach(Mise Lamise in TapisMise)
			{
				foreach(Carte Lacarte in Lamise.CarteMonnaie)
				{
					Lacarte.Selection = false;
					Lacarte.Retourne = false;
					Vainqueur.AddToJeu(Lacarte);
				}
				foreach(Carte Lacarte in Lamise.CarteCachee)
				{
					Lacarte.Selection = false;
					Lacarte.Retourne = false;
					Vainqueur.AddToJeu(Lacarte);
				}
			}
			Vainqueur.AddToJeu(CarteObjectif);
			return Vainqueur.Nom;
		}
		
		
	}
}