using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using NeoAxis;
using NeoAxis.Editor;
using System.Linq;

namespace Project
{
	public class Carte : NeoAxis.Component
	{
		
		DebugInfo DebugInfo = new DebugInfo();
		DebugInfo.DebugMessage Message = new DebugInfo.DebugMessage();
		DebugInfo.ModeAffichage ModeAffichage = DebugInfo.ModeAffichage.Virtuel;
		DebugInfo.NiveauMessage DebugLevel = DebugInfo.NiveauMessage.Debug;
		public enum ObjPrives
		{
			//Moto,
			//Auto,
			//Avion,
			//Maison
			Bateau1,
			Bateau2,
			Bateau3,
			Bateau4,
			Bateau5,
			Bateau6,
			Bateau7,
			Bateau8,
			Bateau9
		}
		public enum ObjCollectifs
		{
			Batiment1,
			Batiment2,
			Batiment3,
			Batiment4,
			Batiment5,
			Batiment6,
			Batiment7
			/*
			Pompier,
			Commissariat,
			Ecole,
			College,
			Lycee,
			Hopital,
			Aeroport*/
		}
		public enum Commentaires
		{
			Commentaire1,
			Commentaire2,
			Commentaire3,
			Commentaire4,
			Commentaire5,
			Commentaire6,
			Commentaire7,
			Commentaire8,
			Commentaire9
			/*Voyage,
			Jardin,
			Gobelet,
			Mur,
			Train,
			Sachet,
			Marteau,
			Tournevis*/
		}
		
		/*public enum MsFiscales
		{
			Voyage,
			Jardin,
			Gobelet,
			Mur,
			Train,
			Sachet,
			Marteau,
			Tournevis
		}*/
		
		public ObjPrives ObjectifPrive { get; set; }
		public ObjCollectifs ObjectifCollectif { get; set; }
		//public MsFiscales  MesureFiscal;
		public int Valeur { get; set; }
		public Commentaires Commentaire { get; set; }
		public string detenteur { get; set; }
		public string place;
		public bool Selection = false;
		public bool Retourne = false;
		Component_Scene Scene;
		
		public void EntityGraphic(Component_Scene Scene )
		{
			
			//Scene = Component_Scene.First;
			if (Scene.Name == "TableJeu")
			{
				//ScreenMessages.Add("Nous sommes dans la bonne scene Carte");
				Component_MeshInSpace FormeGraphique = (Component_MeshInSpace) Scene.GetComponent<Component_MeshInSpace>("Carte");
				//Component_ObjectInSpace FormeGraphique = (Component_ObjectInSpace) Scene.GetComponent<Component_ObjectInSpace>("Carte");
				//FormeGraphique.Enabled = true;
				if (FormeGraphique != null)
				{
					//ScreenMessages.Add("Nous avons la carte");
					Component_MeshInSpace SurfaceCollective = (Component_MeshInSpace) FormeGraphique.GetComponent<Component_MeshInSpace>("ObjectifCollectif");
					Component_MeshInSpace SurfacePrive = (Component_MeshInSpace) FormeGraphique.GetComponent<Component_MeshInSpace>("ObjectifPrive");
					Component_MeshInSpace SurfacePrive2 = (Component_MeshInSpace) FormeGraphique.GetComponent<Component_MeshInSpace>("ObjectifPrive2");
					Component_MeshInSpace SurfaceValeur = (Component_MeshInSpace) FormeGraphique.GetComponent<Component_MeshInSpace>("Valeur");
					Component_MeshInSpace SurfaceCommentaire = (Component_MeshInSpace) FormeGraphique.GetComponent<Component_MeshInSpace>("Commentaire");
					Component_RigidBody Deplacement = (Component_RigidBody) FormeGraphique.GetComponent<Component_RigidBody>("Rigid Body");
					SurfaceCollective.ReplaceMaterial = (Component_Material)LoadMaterial(ObjectifCollectif.ToString(), Scene);
					SurfacePrive.ReplaceMaterial = (Component_Material) LoadMaterial(ObjectifPrive.ToString(), Scene);
					SurfacePrive2.ReplaceMaterial = (Component_Material) LoadMaterial(ObjectifPrive.ToString(), Scene);
					SurfaceValeur.ReplaceMaterial = (Component_Material)  LoadMaterial("Valeur"+ Valeur.ToString(), Scene);
					SurfaceCommentaire.ReplaceMaterial = (Component_Material) LoadMaterial(Commentaire.ToString(), Scene);
				}
				
			}
			
			
		}
		public void PrendreForme(string name, Component_Scene seine)
		{
			
			Scene = seine;
			if (seine.Name == "TableJeu")
			{
				Message.NvMess = DebugInfo.NiveauMessage.Debug;
				Message.DbgMess ="Nous sommes dans la bonne scene Carte";
				DebugInfo.Add(ModeAffichage, Message, DebugLevel);
				Component_MeshInSpace FormeGraphique = (Component_MeshInSpace) Scene.GetComponent<Component_MeshInSpace>(name);
				//Component_ObjectInSpace FormeGraphique = (Component_ObjectInSpace) Scene.GetComponent<Component_ObjectInSpace>(name);
				FormeGraphique.Enabled = true;
				if (FormeGraphique != null)
				{
					//ScreenMessages.Add("Nous avons la carte");
					Component_MeshInSpace SurfaceCollective = (Component_MeshInSpace) FormeGraphique.GetComponent<Component_MeshInSpace>("ObjectifCollectif");
					Component_MeshInSpace SurfacePrive = (Component_MeshInSpace) FormeGraphique.GetComponent<Component_MeshInSpace>("ObjectifPrive");
					Component_MeshInSpace SurfacePrive2 = (Component_MeshInSpace) FormeGraphique.GetComponent<Component_MeshInSpace>("ObjectifPrive2");
					Component_MeshInSpace SurfaceValeur = (Component_MeshInSpace) FormeGraphique.GetComponent<Component_MeshInSpace>("Valeur");
					Component_MeshInSpace SurfaceCommentaire = (Component_MeshInSpace) FormeGraphique.GetComponent<Component_MeshInSpace>("Commentaire");
					Component_RigidBody Deplacement = (Component_RigidBody) FormeGraphique.GetComponent<Component_RigidBody>("Rigid Body");
					SurfaceCollective.ReplaceMaterial = (Component_Material)LoadMaterial(ObjectifCollectif.ToString(), Scene);
					SurfacePrive.ReplaceMaterial = (Component_Material) LoadMaterial(ObjectifPrive.ToString(), Scene);
					SurfacePrive2.ReplaceMaterial = (Component_Material) LoadMaterial(ObjectifPrive.ToString(), Scene);
					SurfaceValeur.ReplaceMaterial = (Component_Material)  LoadMaterial("Valeur"+ Valeur.ToString(), Scene);
					SurfaceCommentaire.ReplaceMaterial = (Component_Material) LoadMaterial(Commentaire.ToString(), Scene);
				}
				
			}
			
			
		}
		private Component_Material LoadMaterial(string nom, Component_Scene gh)
		{
			Component_Material matiere;
			string fileName = "Materials\\" + nom + ".material";
			//string error_string="";
			Message.NvMess = DebugInfo.NiveauMessage.Debug;
			Message.DbgMess ="Nous avons la carte changement " + fileName;
			DebugInfo.Add(ModeAffichage, Message, DebugLevel);
			if (VirtualFile.Exists(fileName))
			{
				matiere = (Component_Material)ResourceManager.LoadSeparateInstance<Component_Material>(fileName, false,true);//, out error_string);
				gh.AddComponent(matiere);
			}
			matiere = (Component_Material)ResourceManager.LoadSeparateInstance<Component_Material>(fileName, false,true);
			if (matiere != null)
			{
				matiere = (Component_Material)gh.GetComponent<Component_Material>(nom);
			}
			return matiere;
			//ResourceManager.LoadSeparateInstance<UIControl>( fileName, false, true )
		}
		/*private Component_Material  AnnuaireTexture(string nom )
		{
			switch(nom)
			{
				case(
			}
		}*/
		
		/*protected override void OnEnabledInSimulation()
		{
			base.OnEnabledInSimulation();
			//EntityGraphic();
		}*/
		/*
		protected override void OnUpdate( float delta )
		{
		}
		
		protected override void OnSimulationStep()
		{
		}
		*/
	}
}